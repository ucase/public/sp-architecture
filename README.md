# SP Architecture

This repository contains the source code of the SP architecture presented
in [[1](https://www.sciencedirect.com/science/article/abs/pii/S092054891930008X)]. This SP application combines the
capabilites of SP, for consuming and processing data, with DSS, for heterogeneous data transmitting, and CEP, for
analysing the heterogeneous data streams already processed. It results in a complete scalable architecture which is able
to handle more than 850 000 events per second using a 4 desktop computers.

## Prerequisites

- You need Java JDK 8 (download it [here](https://www.oracle.com/es/java/technologies/javase/javase-jdk8-downloads.html)).
- You need Maven (download it [here](https://maven.apache.org/install.html)).
- You need a Kafka Server running (download it [here](https://www.apache.org/dyn/closer.cgi?path=/kafka/2.8.0/kafka_2.13-2.8.0.tgz)).
- You have to configure in the file `server.properties` of the `Kafka/config` installation folder, the line
  31 `listeners=PLAINTEXT://localhost:9092`.

## How to run it

1. Clone this repo
2. Start Kafka and create the required topics (`streams-input`, `streams-schemas`, `streams-epl`, `streams-undeploy`
   and `streams-output`). Use the `commands.txt` file in order to start `Zookeeper`, `Kafka` and create the topics. You
   can run the script `start.bat` in a Windows PowerShell to perform those actions (but you will have to customise the
   paths to your owns).
    - First, start, in new terminal, `Zookeeper` with the following
      command: `bin\windows\zookeeper-server-start.bat config\zookeeper.properties`
    - Second, start, in a new terminal, `Kafka` with the following
      command: `bin\windows\kafka-server-start.bat config\server.properties`
    - Third, create the 5 required topics, in a new terminal, with the following template
      command: `bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-input`
    - Note that, all these terminals must be opened in the root folder of the Kafka installation. You can find more info
      about these commands at [[2]](https://kafka.apache.org/quickstart)
3. Compile the project in a single `JAR` with `mvn package`.
4. Start the `JAR`, which has been generated inside the `target` folder, file using the command below:
    * `java -jar sp-architecture.jar`
    * Alternatively, you can run `Main.java` in an IDE.

At this point, your app should connect to `Kafka` and, in the console, you should read something like:

```
INFO  Main:37 - * INITIALIZING CONSTANTS *
INFO  Main:58 - * STARTING STREAMING APP *
INFO  Main:61 - * INITIALIZING ESPER CEP ENGINE *
INFO  EsperUtils:42 - ** Starting Esper Engine **
INFO  EsperUtils:45 - *** Creating configuration ***
INFO  EsperUtils:50 - *** Starting Esper Compiler ***
INFO  EsperUtils:55 - *** Starting Esper Runtime ***
INFO  Main:24 - ** Starting Data Processor 0_0 **
```

If you can't see the last line `** Starting Data Processor 0_0 **`, please check down below at `Troubleshoot` section.

At this point, your app is properly running, so you can start analysing some data:

5. Start a producer and send data (simple events) to the `streams-input` topic:
    - Open a new terminal and run the
      command: `bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic streams-input`
    - Send the following `Dummy` simple event: `Dummy,10,this is a dummy event`
6. Start a producer and send patterns to the `streams-epl` topic.
    - Open a new terminal and run the
      command: `bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic streams-epl`
    - Send the following event pattern: `@NAME("Bigger than 5") SELECT * FROM Dummy WHERE p2 > 5`
7. Start a consumer on `streams-output` in order to see the complex events detected (you can see them also at the
   terminal output of your app):
    - Open a new terminal (or use the previous one), and run the
      command: `bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --from-beginning --topic streams-output`
8. Send some simple events in order to trigger the pattern previously specified.
    - Open a new terminal (or use the same from the step nº5) and run the
      command: `bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic streams-input`
    - Send the following `Dummy` event: `Dummy,50,this is a dummy event`
        - As the `p1` is now `50`, and `50` is greater than `5`, the pattern that we sent at step nº6 will be triggered.
          If you send the following simple event `Dummy,4,this is a dummy event`, `4` is not greater than `5`, so no
          complex event will be detected.

## Topics

- ``streams-input`` Topic to receive simple events (the information to be analysed). This information can be `JSON`
  , `XML` or `CSV` rows without headers.
- ``streams-schemas`` Topic to receive schemas from MEdit4CEP-SP.
- ``streams-epl`` Topic to receive EPL patterns.
- ``streams-undeploy`` Topic to receive ids to undeploy from CEP engine.
- ``streams-output`` Topic to store the complex events detected.

## FYI

- If you are using `Windows`, I would recommend you to
  install [Windows Terminal](https://www.microsoft.com/es-es/p/windows-terminal/9n0dx20hk701?activetab=pivot:overviewtab)
  that allows you to have a single terminal with multiple tabs. It is very comfortable.
- If you are using `Linux`, I would recommend you to
  install [Terminator](https://terminator-gtk3.readthedocs.io/en/latest/) that allows you to have a single terminal with
  multiple tabs. It is very comfortable.
- Change `log4j.appender.file.File` in `log4j.properties` file, at `resources` folder. Currently, is writing logs on the
  same folder that the JAR file is or in the projects' folder.
- Modify `app.properties` file parameters, at `resources` folder, in order to customize your execution.

## Troubleshoot

- It is recommended to run `Zookeeper` and `Kafka` in terminal with admin privileges.
- If you can't see the last line `** Starting Data Processor 0_0 **` it means that your app is not able to connect
  to `Kafka`. Please check the file `app.properties` at the `resources` folder in order to make sure that you are
  connecting to the correct IP (most likely `localhost:9092`).
- When facing strange issues, remove the `logs` folders created by `Zookeeper` and `Kafka`.

## Docker

There is the possibility of running this whole architecture inside of Docker containers:

1. In the file `app.properties`, change the `bootstrap-servers` property to `kafka:9092`.
2. Open a terminal at the root of the project and run the command `docker-compose up`. This will run the configuration
   specified at the `docker-compose.yml` file.
3. Your can create console producers and consumers, as those at the steps number 5, 6, and 7, but, in this case, the
   port will be `9094` instead of `9092`, because is the one exposed to our current machine (or the machine in which the
   docker containers are running).

## Versions

Actually is running properly with `Esper` 8.7.0 and `Kafka` 2.8.0.
