package serde;

import com.google.gson.Gson;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.kafka.common.serialization.Deserializer;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.json.XML;
import utils.AvroConverter;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import java.io.Closeable;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static utils.Constants.eventTypeName;
import static utils.Constants.eventTypeNamePosition;
import static utils.Constants.rawDelimiter;
import static utils.Constants.inputTopicEvents;
import static utils.Constants.inputTopicSchemas;
import static utils.Constants.inputTopicEPL;
import static utils.Constants.inputTopicUndeploy;


public class EventDeserializer implements Closeable, AutoCloseable, Deserializer<GenericData.Record> {

    private static final HashMap<String, Schema> schemasDictionary = new HashMap<>();
    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static final JsonAvroConverter converter = new JsonAvroConverter();
    private static final Schema.Field eplStatement = new Schema.Field("epl",
            Schema.create(Schema.Type.STRING), "EPL statement", (Object) null);
    private static final Schema.Field mongodbId = new Schema.Field("mongodbId",
            Schema.create(Schema.Type.STRING), "MongoDB ID", (Object) null);
    private static final Schema eplSchema = Schema.createRecord("epl", "epl",
            "", false, Arrays.asList(eplStatement, mongodbId));
    private static final Schema.Field undeployIds = new Schema.Field("undeployIds",
            Schema.create(Schema.Type.STRING), "Undeploy ID", (Object) null);
    private static final Schema undeployIdsSchema = Schema.createRecord("undeploy", "undeploy",
            "", false, Collections.singletonList(undeployIds));

    @Override
    public void configure(Map<String, ?> map, boolean b) {
    }

    @Override
    public GenericData.Record deserialize(String topic, byte[] bytes) {
        GenericData.Record record = null;
        String event = new String(bytes, CHARSET);
        char first = event.charAt(0);

        if (topic.equals(inputTopicEvents)) {
            boolean isRaw = false;

            if (first != '{' && first != '<') {
                isRaw = true;
                event = convertRAWtoJSON(event);
            } else if (first == '<') {
                event = XML.toJSONObject(event).toString();
            }

            try {
                Schema eventSchema = null;
                String eventTypeNameValue = getEventTypeName(event, eventTypeNamePosition, isRaw);
                if (!schemasDictionary.containsKey(eventTypeNameValue)) {
                    try {
                        String stringSchema = AvroConverter.convert(event, eventTypeName,
                                eventTypeNamePosition, isRaw);
                        eventSchema = new Schema.Parser().parse(stringSchema);
                        eventSchema.addAlias("event");
                        schemasDictionary.put(eventSchema.getName(), eventSchema);
                    } catch (IOException e) {
                        System.out.println("Exception creating Avro Schema" + e);
                    }
                }
                record = converter.convertToGenericDataRecord(event.getBytes(), schemasDictionary.get(eventTypeNameValue));
            } catch (Exception e) {
                throw new IllegalArgumentException("Error reading bytes", e);
            }
        } else if (topic.equals(inputTopicSchemas)) {
            Schema eventSchema = new Schema.Parser().parse(event);
            eventSchema.addAlias("schema");
            record = new GenericData.Record(eventSchema);
        } else if (topic.equals(inputTopicEPL)) {
            eplSchema.addAlias("epl");
            record = new GenericData.Record(eplSchema);
            if (event.length() > 24 && new ObjectId().isValid(event.substring(0, 24))) {
                record.put("mongodbId", event.substring(0, 24));
                record.put("epl", event.substring(24));
            } else {
                record.put("mongodbId", null);
                record.put("epl", event);
            }
        } else if (topic.equals(inputTopicUndeploy)) {
            undeployIdsSchema.addAlias("undeploy");
            record = new GenericData.Record(undeployIdsSchema);
            record.put("undeployIds", event);
        }

        return record;
    }

    @Override
    public void close() {

    }

    /**
     * This function retrieves the event type name value of the event which is being processed
     *
     * @param event                 the event to process as JSON string
     * @param eventTypeNamePosition the event type name value position in the JSON string
     * @param isRaw                 a flag to determinate if the event was RAW data before its conversion to JSON string
     * @return the event type name value
     */
    public String getEventTypeName(String event, int eventTypeNamePosition, boolean isRaw) {
        JSONObject json = new JSONObject(event);
        if (eventTypeNamePosition >= 0 && isRaw)
            return json.getString("p" + (eventTypeNamePosition + 1));
        else
            return json.has(eventTypeName) ? json.getString(eventTypeName) : null;
    }

    /**
     * @param raw is the raw data which has to be converted into a generic JSON
     * @return the raw data as an structured generic JSON
     */
    private static String convertRAWtoJSON(String raw) {
        List<String> valuesList = Arrays.asList(raw.split(rawDelimiter));
        LinkedHashMap<String, Object> valuesHash = new LinkedHashMap<>();
        int n = valuesList.size();
        for (int i = 0; i < n; i++) {
            valuesHash.put("p" + (i + 1), getType(valuesList.get(i)));
        }
        return new Gson().toJson(valuesHash);
    }

    /**
     * @param value is the String value which we have to determine its real type
     * @return the type of this string value, could be: integer, long, float, double or string
     */
    private static Object getType(String value) {
        if (NumberUtils.isCreatable(value)) {
            try {
                return NumberUtils.createInteger(value);
            } catch (NumberFormatException e) {
                try {
                    return NumberUtils.createLong(value);
                } catch (NumberFormatException e1) {
                    try {
                        return NumberUtils.createFloat(value);
                    } catch (NumberFormatException e2) {
                        return value;
                    }
                }
            }
        } else if (BooleanUtils.toBooleanObject(value) == null && !value.equals("null"))
            return value;
        else
            return BooleanUtils.toBooleanObject(value);
    }
}