package serde;

import org.apache.avro.generic.GenericData;
import org.apache.kafka.common.serialization.Serializer;

import java.io.Closeable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class EventSerializer implements Closeable, AutoCloseable, Serializer<GenericData.Record> {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    @Override
    public void configure(Map<String, ?> map, boolean b) {
    }

    @Override
    public byte[] serialize(String s, GenericData.Record event) {
        String line = event.toString();
        return line.getBytes(CHARSET);
    }

    @Override
    public void close() {

    }
}