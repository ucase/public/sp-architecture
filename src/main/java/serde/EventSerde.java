package serde;

import org.apache.avro.generic.GenericData;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class EventSerde implements Serde<GenericData.Record> {
    private final EventSerializer serializer = new EventSerializer();
    private final EventDeserializer deserializer = new EventDeserializer();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<GenericData.Record> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<GenericData.Record> deserializer() {
        return deserializer;
    }
}
