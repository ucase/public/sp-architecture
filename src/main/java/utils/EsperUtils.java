package utils;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.common.client.dataflow.core.EPDataFlowInstance;
import com.espertech.esper.common.internal.event.avro.AvroSchemaEventType;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompiler;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.*;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;

import java.util.*;

import static utils.Constants.*;

/**
 * @author David Corral-Plaza <david.corral@uca.es>
 */

public class EsperUtils {

    private final static Logger logger = Logger.getLogger(EsperUtils.class);
    private static Producer<Long, String> kafkaProducer;
    private static EPCompiler epCompiler;
    private static EPRuntime epRuntime;
    private static Configuration configuration;

    /**
     * Constructor
     */
    public EsperUtils() {
        synchronized (EsperUtils.class) {
            logger.info("** Starting Esper Engine **");

            if (configuration == null) {
                logger.info("*** Creating configuration ***");
                configuration = new Configuration();
            }

            if (epCompiler == null) {
                logger.info("*** Starting Esper Compiler ***");
                epCompiler = EPCompilerProvider.getCompiler();
            }

            if (epRuntime == null) {
                logger.info("*** Starting Esper Runtime ***");
                epRuntime = EPRuntimeProvider.getDefaultRuntime(configuration);
            }

        }
    }

    /**
     * Returns the CompilerArguments each time
     * that a EPL code has to be compiled
     *
     * @return arguments with the global configuration
     */
    public static CompilerArguments getCompilerArguments() {
        CompilerArguments arguments = new CompilerArguments(getEpRuntime().getRuntimePath());
        arguments.setConfiguration(configuration);
        return arguments;
    }

    /**
     * Returns the epCompiler that have been set before
     *
     * @return the epCompiler of the engine
     */
    public static EPCompiler getEpCompiler() {
        synchronized (EsperUtils.class) {
            if (epCompiler == null) {
                logger.info("epCompiler is not defined");
                throw new RuntimeException("Unable to continue because epCompiler is not defined!");
            }
        }
        return epCompiler;
    }

    /**
     * Returns the epRuntime that have been set before
     *
     * @return the epRuntime of the engine
     */
    public static EPRuntime getEpRuntime() {
        synchronized (EsperUtils.class) {
            if (epRuntime == null) {
                logger.info("epRuntime is not defined");
                throw new RuntimeException("Unable to continue because epRuntime is not defined!");
            }
        }
        return epRuntime;
    }

    /**
     * Returns the deployment id that have been set before
     *
     * @return the epDeploymentId used at runtime
     */
    public static String getDeploymentId() {
        logger.info("Deployments Ids: " + Arrays.asList(getEpRuntime().getDeploymentService().getDeployments()));
        return getEpRuntime().getDeploymentService().getDeployments()[0];
    }

    /**
     * Add a generic listener to each EPL statement
     *
     * @param statement the EPStatement which will have the listener added
     */
    private static void addGenericListener(EPStatement statement) {
        statement.addListener((newComplexEvents, oldComplexEvents, detectedEventPattern, epRuntime) -> {
            if (newComplexEvents != null) {
                String eventPatternName = detectedEventPattern.getEventType().getName();
                logger.info("** Complex event '" + eventPatternName + "' detected: " + newComplexEvents[0].getUnderlying());
                publishKafka(newComplexEvents[0].getUnderlying().toString(), outputTopic);
            }
        });
    }

    /**
     * Receives and EPL and add it to the Esper Engine with a generic listener
     *
     * @param epl the EPL to be added to the engine
     * @throws EPCompileException fail compiling the EPL
     * @throws EPDeployException  fail deploying the EPL
     */
    public static String createEPL(String epl) throws EPCompileException, EPDeployException {
        //addGenericListener(getEpRuntime().getDeploymentService().getStatement(deployNewEventPattern(epl).getDeploymentId(), name));
        EPStatement ep = deployNewEventPattern(epl).getStatements()[0];
        addGenericListener(ep);
        return ep.getDeploymentId();
    }

    /**
     * Receives a string with all ids and iterates for undeploy them
     *
     * @param ids The ids as String to be undeployed
     */
    public static void undeployIds(String ids) {
        String[] deploymentIds = ids.replaceAll(" ", "").split(",");
        for (String id : deploymentIds) {
            undeploy(id);
        }
    }

    /**
     * Receives an ArrayList with all ids and iterates for undeploy them
     *
     * @param deploymentIds The ids as ArrayList to be undeployed
     */
    public static boolean undeployIds(ArrayList<String> deploymentIds) {
        for (String id : deploymentIds) {
            undeploy(id);
        }
        return true;
    }

    /**
     * Auxiliary function to undeploy ids
     */
    private static boolean undeploy(String id) {
        logger.info("** Undeploying " + id);
        try {
            getEpRuntime().getDeploymentService().undeploy(id);
            logger.info("** " + id + " undeployed successfully");
            return true;
        } catch (EPUndeployException e) {
            String deploymentId = e.getMessage().substring(e.getMessage().length() - 37, e.getMessage().length() - 1);
            if (deploymentId.length() == 36) {
                logger.warn("*** There is a precondition that has to be undeployed first: " + deploymentId);
                ArrayList<String> deploymentIds = new ArrayList<>();
                deploymentIds.add(deploymentId);
                deploymentIds.add(id);
                return undeployIds(deploymentIds);
            } else
                return true;
        }
    }

    /**
     * Creates a new dataflow
     *
     * @param epl  The dataflow to be deployed
     * @param name The dataflow's name
     * @throws EPCompileException fail compiling the dataflow
     * @throws EPDeployException  fail deploying the dataflow
     */
    public static void createDataflow(String epl, String name) throws EPCompileException, EPDeployException {
        logger.info("Deploying dataflow '" + name + "'");
        EPDataFlowInstance instance =
                getEpRuntime().getDataFlowService().instantiate(deployNewEventPattern(epl).getDeploymentId(), name);

        logger.info("Dataflow '" + name + "' instantiated");
        instance.run();
        logger.info("Dataflow '" + name + "' finished");
    }

    /**
     * Calls to the compiler and then deploy the compiled EPL sentence
     *
     * @param epl The EPL sentence to be compiled
     * @return The EPL sentence deployed
     * @throws EPCompileException fail compiling the EPL
     * @throws EPDeployException  fail deploying the EPL
     */
    public static EPDeployment deployNewEventPattern(String epl) throws EPCompileException, EPDeployException {
        return getEpRuntime().getDeploymentService().deploy(compileNewEventPattern(epl));
    }

    /**
     * Compiles an EPL sentence using the runtime configuration
     *
     * @param epl The EPL sentence to be compiled
     * @return The EPL sentence compiled
     * @throws EPCompileException fail compiling the EPL
     */
    public static EPCompiled compileNewEventPattern(String epl) throws EPCompileException {
        return getEpCompiler().compile(epl, getCompilerArguments());
    }

    /**
     * Creates a new event type at runtime
     *
     * @param epl The sentence with the new schema to create
     * @throws EPCompileException fail compiling the Schema
     * @throws EPDeployException  fail deploying the Schema
     */
    public static String addNewSchema(String epl) throws EPCompileException, EPDeployException {
        logger.info("Adding new schema: " + epl);
        return deployNewEventPattern(epl).getDeploymentId();
    }

    /**
     * Checks if the event type name is already in use
     *
     * @param eventTypeName to check if exists
     * @return if the event type name exists or not
     */
    public static boolean eventTypeExists(String eventTypeName) {
        for (String deploymentId : getEpRuntime().getDeploymentService().getDeployments())
            if (getEpRuntime().getEventTypeService().getEventType(deploymentId, eventTypeName) != null)
                return true;
        return false;
    }

    /**
     * Adds a new Avro Event to the Esper engine
     *
     * @param avroEvent     the Avro Event already created
     * @param eventTypeName the event type name of the Avro Event
     */
    public static void sendAvroEvent(GenericData.Record avroEvent, String eventTypeName) {
        getEpRuntime().getEventService().sendEventAvro(avroEvent, eventTypeName);
    }

    /**
     * Extracts the Schema from an Avro Event already registered in the engine
     *
     * @param eventTypeName the event type name of the event type to extract the schema
     * @return the Schema of the event type
     */
    static Schema getAvroSchema(String eventTypeName) {
        return (Schema) ((AvroSchemaEventType)
                getEpRuntime().getEventTypeService().getEventType(getDeploymentId(), eventTypeName)).getSchema();
    }

    /**
     * Set up default options
     * It is useful for testing purposes
     */
    public static void setDefaults() throws EPCompileException, EPDeployException {
        addNewSchema("@public @buseventtype @EventRepresentation(avro) create avro schema WaterMeasurement as (serialNumber string, dateTime string, volumeM3 double, volumeL double, type string, batteryLevel int, batteryLevelStr string, sleepingTime integer, leakingTime integer, normalTime integer, starts integer)");
        deployNewEventPattern("@public create context IntervalSpanning300Seconds start @now end after 300 sec");
        createEPL("context IntervalSpanning300Seconds select count(*) from WaterMeasurement output snapshot when terminated");
    }

    /*
    private static void setDefaults() {
        String schema = "{\"type\":\"record\",\"name\":\"WaterMeasurement\",\"fields\":[{\"name\":\"p12\",\"type\":\"string\"},{\"name\":\"p1\",\"type\":\"string\"},{\"name\":\"p11\",\"type\":\"string\"},{\"name\":\"p2\",\"type\":\"string\"},{\"name\":\"p3\",\"type\":\"string\"},{\"name\":\"p4\",\"type\":\"string\"},{\"name\":\"p5\",\"type\":\"string\"},{\"name\":\"p6\",\"type\":\"string\"},{\"name\":\"p7\",\"type\":\"string\"},{\"name\":\"p8\",\"type\":\"string\"},{\"name\":\"p9\",\"type\":\"string\"},{\"name\":\"p10\",\"type\":\"string\"}]}";
        Schema eventSchema = new Schema.Parser().parse(schema);
        addNewAvroEventType("WaterMeasurement", eventSchema);
        String epl = "@Name('Time-Batch') SELECT count(*) FROM WaterMeasurement#time_batch(600 sec)";
        createEPL(epl);
    }

    private static void setDefaults2(){
        String schema = "{\"type\":\"record\",\"fields\":[{\"name\":\"p12\",\"type\":\"string\"},{\"name\":\"p1\",\"type\":\"string\"},{\"name\":\"p11\",\"type\":\"double\"},{\"name\":\"p2\",\"type\":\"string\"},{\"name\":\"p3\",\"type\":\"string\"},{\"name\":\"p4\",\"type\":\"double\"},{\"name\":\"p5\",\"type\":\"double\"},{\"name\":\"p6\",\"type\":\"string\"},{\"name\":\"p7\",\"type\":\"double\"},{\"name\":\"p8\",\"type\":\"double\"},{\"name\":\"p9\",\"type\":\"double\"},{\"name\":\"p10\",\"type\":\"double\"}],\"name\":\"WaterMeasurement\"}";
        Schema eventSchema = new Schema.Parser().parse(schema);
        //addNewAvroEventType("WaterMeasurement", eventSchema);
        //addNewEventPattern("create context IntervalSpanning300Seconds start @now end after 300 sec");
        //createEPL("context IntervalSpanning300Seconds select count(*) from WaterMeasurement output snapshot when terminated");
        ConfigurationEventTypeAvro eventTypeAvro = new ConfigurationEventTypeAvro(eventSchema);
        getEpRuntime().getEPAdministrator().getConfiguration().addEventTypeAvro("WaterMeasurement", eventTypeAvro);
        getEpRuntime().getEPAdministrator().createEPL(" create dataflow ConsumingFromKafka \n" +
                " EventBusSource -> WaterMeasurementInputStream<WaterMeasurement> {} \n" +
                " Select(WaterMeasurementInputStream) -> outstream {\n" +
                "    select: (select count(*) from WaterMeasurementInputStream)\n" +
                "  } \n" +
                " LogSink(outstream) {}");
        EPDataFlowInstance instance =
                epRuntime.getEPRuntime().getDataFlowRuntime().instantiate("ConsumingFromKafka");
        instance.run();
    }
    */

    /*
    /**
     * Set up again the generic listeners for all EPL statements
     */
    /*
    private static void setListeners() {
        synchronized (EsperUtils.class) {
            if (epRuntime == null) {
                logger.error("epRuntime is not defined");
                throw new RuntimeException("Unable to continue because epRuntime is not defined!");
            } else {
                String[] statements = epRuntime.getEPAdministrator().getStatementNames();
                for (String statementName : statements) {
                    logger.info("** Setting up listener for: " + statementName);
                    EPStatement statement = epRuntime.getEPAdministrator().getStatement(statementName);
                    addGenericListener(statement);
                }
            }
        }
    }
    */

    /**
     * Send the complex event detected to the topic specified and also
     * calls to the method to add a generic listener
     *
     * @param event the event detected
     * @param topic the topic where the event will be sent
     */
    public static void publishKafka(String event, String topic) {
        if (kafkaProducer == null) {
            Properties props = new Properties();
            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
            props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaJavaProducer");
            props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
            kafkaProducer = new KafkaProducer<>(props);
        }

        Producer<Long, String> producer = kafkaProducer;

        try {
            logger.info("Sending the message <" + event + "> to " + topic);
            ProducerRecord<Long, String> record = new ProducerRecord<>(topic, event);
            producer.send(record);
        } catch (Exception e) {
            logger.error("Exception sending complex event to Kafka topic", e);
        }
    }
}

