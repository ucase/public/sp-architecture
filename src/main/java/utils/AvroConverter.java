package utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.JsonDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author swapnil.marghade on 07/12/15.
 * Modified by David Corral.
 */
public class AvroConverter {

    private static final Logger logger = LoggerFactory.getLogger(AvroConverter.class);

    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String ARRAY = "array";
    private static final String ITEMS = "items";
    private static final String STRING = "string";
    private static final String RECORD = "record";
    private static final String FIELDS = "fields";

    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * Convert to avro schema
     * @param json to convert
     * @return avro schema json
     * @throws IOException
     */
    public static String convert(final String json, final String eventTypeName, final int eventTypeNamePos, final boolean isRaw) throws IOException {
        final JsonNode jsonNode = mapper.readTree(json);
        final ObjectNode finalSchema = mapper.createObjectNode();
        finalSchema.put(TYPE, RECORD);
        finalSchema.set(FIELDS, getFields(jsonNode));
        if (eventTypeNamePos >= 0 && isRaw)
            finalSchema.put(NAME, jsonNode.get("p" + (eventTypeNamePos + 1)).textValue());
        else
            finalSchema.put(NAME, jsonNode.has(eventTypeName) ? jsonNode.get(eventTypeName).textValue() : getAutomaticName(finalSchema));

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(finalSchema);
    }

    /**
     * @param jsonNode to getFields
     * @return array nodes of fields
     */
    private static ArrayNode getFields(final JsonNode jsonNode) {
        final ArrayNode fields = mapper.createArrayNode();
        final Iterator<Map.Entry<String, JsonNode>> elements = jsonNode.fields();

        Map.Entry<String, JsonNode> map;
        while (elements.hasNext()) {
            map = elements.next();
            final JsonNode nextNode = map.getValue();

            switch (nextNode.getNodeType()) {
                case NUMBER:
                    fields.add(mapper.createObjectNode().put(NAME, map.getKey())
                            .put(TYPE, (nextNode.isInt() ? "int" : nextNode.isLong() ? "long" : nextNode.isFloat() ? "float" : "double")));
                    break;
                case STRING:
                    fields.add(mapper.createObjectNode().put(NAME, map.getKey())
                            .put(TYPE, STRING));
                    break;
                case ARRAY:
                    final ArrayNode arrayNode = (ArrayNode) nextNode;
                    final JsonNode element = arrayNode.get(0);
                    final ObjectNode objectNode = mapper.createObjectNode();
                    objectNode.put(NAME, map.getKey());

                    if (element.getNodeType() == JsonNodeType.NUMBER) {
                        objectNode.set(TYPE, mapper.createObjectNode().put(TYPE, ARRAY)
                                .put(ITEMS, (nextNode.isInt() ? "int" : nextNode.isLong() ? "long" : nextNode.isFloat() ? "float" : "double")));
                        fields.add(objectNode);
                    } else if (element.getNodeType() == JsonNodeType.STRING) {
                        objectNode.set(TYPE, mapper.createObjectNode().put(TYPE, ARRAY).put(ITEMS, STRING));
                        fields.add(objectNode);
                    } else {
                        objectNode.set(TYPE, mapper.createObjectNode().put(TYPE, ARRAY).set(ITEMS, mapper.createObjectNode()
                                .put(TYPE, RECORD).put(NAME, generateRandomNumber(map)).set(FIELDS, getFields(element))));
                    }
                    fields.add(objectNode);
                    break;
                case OBJECT:
                    ObjectNode node = mapper.createObjectNode();
                    node.put(NAME, map.getKey());
                    node.set(TYPE, mapper.createObjectNode().put(TYPE, RECORD).put(NAME, generateRandomNumber(map)).set(FIELDS, getFields(nextNode)));
                    fields.add(node);
                    break;
                default:
                    logger.error("Node type not found - " + nextNode.getNodeType());
                    throw new RuntimeException("Unable to determine action for node type " + nextNode.getNodeType() + "; Allowed types are ARRAY, STRING, NUMBER, OBJECT");
            }
        }
        return fields;
    }

    /**
     * @param map to create random number
     * @return random
     */
    private static String generateRandomNumber(Map.Entry<String, JsonNode> map) {
        return (map.getKey() + "_" + new Random().nextInt(100));
    }

    /**
     * @param schema the schema of the event that we want to give a name
     * @return new unique name for schema or existing name if schema already exists
     */
    private static String getAutomaticName(ObjectNode schema) {
        String name = "genericEvent";
        schema.put(NAME, name);
        int i = 0;
        while (EsperUtils.eventTypeExists(name)) {
            Schema existingSchema = EsperUtils.getAvroSchema(name);
            Schema auxSchema = new Schema.Parser().parse(schema.toString());
            if (compareAttributes(existingSchema.getFields(), auxSchema.getFields())) {
                name = existingSchema.getName();
                break;
            } else {
                name = "genericEvent" + i;
                i++;
            }
        }
        return name;
    }

    // Simple alternative
    /*
    private String getAutomaticName(ObjectNode schema) {
        String name = "genericEvent";
        int i = 0;
        while (utils.EsperUtils.eventTypeExists(name)) {
            name = "genericEvent" + i;
            i++;
        }

        return name;
    }
    */

    /**
     * @param first  first list of fields of existing eventType
     * @param second second list of fields of the eventType schema that we are creating right now
     * @return number of different fields (order matters) between this schemas
     */
    private static boolean compareAttributes(List<Schema.Field> first, List<Schema.Field> second) {
        if (first.size() != second.size())
            return false;
        else {
            int n = first.size();
            for (int i = 0; i < n; i++) {
                if (!first.get(i).equals(second.get(i)))
                    return false;
            }
        }
        return true;
    }

    /**
     * Validation
     *
     * @param avroSchemaString to validate
     * @param jsonString       to validate
     * @return true if validated, false otherwise
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public boolean validate(final String avroSchemaString, final String jsonString) throws IOException {
        Schema.Parser t = new Schema.Parser();
        Schema schema = t.parse(avroSchemaString);
        GenericDatumReader reader = new GenericDatumReader(schema);
        JsonDecoder decoder = DecoderFactory.get().jsonDecoder(schema, jsonString);
        reader.read(null, decoder);
        return true;
    }
}