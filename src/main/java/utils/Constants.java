package utils;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

public class Constants {

    public static String inputTopicEvents, inputTopicSchemas, inputTopicEPL, inputTopicUndeploy, outputTopic,
            eventTypeName, applicationId, consumerId, bootstrapServers, rawDelimiter, eqcStore, APIHost, APIKey;
    public static int eventTypeNamePosition;

    public Constants() {
        Configurations configs = new Configurations();
        try {
            Configuration config = configs.properties(new File("app.properties"));

            inputTopicEvents = config.getString("input-topic-events");
            inputTopicSchemas= config.getString("input-topic-schemas");
            inputTopicEPL = config.getString("input-topic-epl");
            inputTopicUndeploy = config.getString("input-topic-undeploy");
            outputTopic = config.getString("output-topic");
            eqcStore = config.getString("eqc-store");

            eventTypeName = config.getString("event-type-name-attribute");
            eventTypeNamePosition = config.getInt("event-type-name-position");

            applicationId = config.getString("application-id");
            consumerId = config.getString("consumer-id");
            bootstrapServers = config.getString("bootstrap-servers");
            rawDelimiter = config.getString("raw-delimiter");

            APIHost = config.getString("api-host");
            APIKey = config.getString("api-key");
        } catch (ConfigurationException cex) {
            throw new RuntimeException("There is no configuration file. Please check the docs and fix this. Stopping execution!");
        }
    }

}
