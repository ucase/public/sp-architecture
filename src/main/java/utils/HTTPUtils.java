package utils;

import okhttp3.*;
import org.apache.avro.Schema;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static processors.Main.logger;
import static utils.Constants.APIHost;
import static utils.Constants.APIKey;

public class HTTPUtils {

    private static OkHttpClient client;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void addNewSimpleEventAPI(Schema schema, String deploymentId) {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
        }

        JSONObject json = new JSONObject();
        json.put("schema", new JSONObject(schema.toString()));
        json.put("deploymentId", deploymentId);
        json.put("domain", "null");
        json.put("timestamp", String.valueOf(System.currentTimeMillis()));

        RequestBody body = RequestBody.create(JSON, json.toString());

        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + APIKey)
                .url(APIHost + "/eventTypes")
                .post(body)
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            if (!response.isSuccessful())
                logger.error("There was an error while publishing the event type in the API. Please try again.");
            else
                logger.info("POST event type successful.");
            response.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //EsperUtils.publishKafka(deploymentId + ";" + schema.toString(), "streams-nodejs");
    }

    public static void addNewPatternAPI(Schema schema, String deploymentId, String epl) {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
        }

        JSONObject json = new JSONObject();
        json.put("name", schema.getName());
        json.put("pattern", epl.replaceAll("\"", "\\\\\"").replaceAll("\\r", " ").replaceAll("\\n", " "));
        json.put("dependencies", new JSONArray());
        json.put("deploymentId", deploymentId);
        json.put("timestamp", String.valueOf(System.currentTimeMillis()));

        RequestBody body = RequestBody.create(JSON, json.toString());

        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + APIKey)
                .url(APIHost + "/patterns")
                .post(body)
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            if (!response.isSuccessful())
                logger.error("There was an error while publishing the event pattern in the API. Please try again.");
            else
                logger.info("POST pattern successful.");
            response.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //EsperUtils.publishKafka(deploymentId + ";" + schema.toString(), "streams-nodejs");
    }

    public static void updateDeploymentId(String path, String id, String deploymentId, String code) {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
        }

        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + APIKey)
                .url(APIHost + "/" + path + "/" + id + "/deploymentId/" + deploymentId)
                .put(RequestBody.create(null, ""))
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                logger.error("An error occurs in the API when updating the DeploymentID of the " + path + ". Please check the API log and try again.");
            } else {
                logger.info(path + " DeploymentID has been set.");
            }

            response.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        //EsperUtils.publishKafka(deploymentId + ";" + code, "streams-nodejs");
    }
}
