package processors;

import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import utils.EsperUtils;
import utils.HTTPUtils;

import java.util.StringJoiner;

import static processors.Main.logger;

public class DataProcessor implements Processor<String, GenericData.Record> {

    //public static AtomicInteger counter = new AtomicInteger(0);
    private ProcessorContext context;

    @Override
    public void init(ProcessorContext processorContext) {
        String idProcessor = processorContext.taskId().toString();
        logger.info("** Starting Data Processor " + idProcessor + " **");
        context = processorContext;
    }

    @Override
    public void process(String key, GenericData.Record event) {
        Schema schema = event.getSchema();
        String eventTypeName = schema.getName();

        if (!EsperUtils.eventTypeExists(eventTypeName)) {
            String epl = "@public @buseventtype @EventRepresentation(avro) create avro schema " + eventTypeName + " as ";
            StringJoiner joiner = new StringJoiner(",");

            for (Schema.Field field : schema.getFields()) {
                joiner.add(field.name() + " " + field.schema().getName());
            }

            epl += "(" + joiner.toString() + ")";

            try {
                //EsperUtils.addNewSchema(epl);
                HTTPUtils.addNewSimpleEventAPI(schema, EsperUtils.addNewSchema(epl));
            } catch (EPCompileException | EPDeployException e) {
                e.printStackTrace();
            }
        }

        EsperUtils.sendAvroEvent(event, eventTypeName);
        /*if (counter.incrementAndGet() % 500000 == 0) {
            logger.info("Events processed " + idProcessor + ": " + counter.get());
        }*/
    }

    @Override
    public void close() {
        logger.info("** Closing Data Processor " + context.taskId() + " **");
    }

}
