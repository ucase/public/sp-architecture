package processors;

import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.log4j.Logger;
import serde.EventDeserializer;
import serde.EventSerde;
import utils.Constants;
import utils.EsperUtils;
import utils.HTTPUtils;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.concurrent.CountDownLatch;

import static utils.Constants.*;

/**
 * @author David Corral-Plaza <david.corral@uca.es>
 */

public class Main {

    final public static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("* INITIALIZING CONSTANTS *");
        new Constants();

        final Topology builder = new Topology()
                .addSource("Source-Events", inputTopicEvents)
                .addProcessor("Data-Processor", DataProcessor::new, "Source-Events")
                .addSink("Sink", outputTopic, "Data-Processor");

        final KafkaStreams streams = new KafkaStreams(builder, getConfiguration());

        final CountDownLatch latch = new CountDownLatch(1);

        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            logger.info("* STARTING STREAMING APP *");
            streams.start();
            startConsumers();
            logger.info("* INITIALIZING ESPER CEP ENGINE *");
            new EsperUtils();
            //EsperUtils.setDefaults();
            latch.await();
        } catch (Throwable e) {
            logger.error("* STOPPING STREAMING APP *");
            logger.error("ERROR: " + e);
            System.exit(1);
        }

        System.exit(0);
    }

    /**
     * Simple consumer for EPL patterns, Avro EPL and Deployment IDs
     */
    public static void startConsumers() {
        new Thread(() -> {
            Consumer<String, GenericData.Record> consumer = new KafkaConsumer<>(getConfiguration());
            consumer.subscribe(Arrays.asList(inputTopicEPL, inputTopicSchemas, inputTopicUndeploy));
            while (true) {
                ConsumerRecords<String, GenericData.Record> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, GenericData.Record> record : records)
                    if (record.value().getSchema().getAliases().contains("schema")) {
                        Schema schema = record.value().getSchema();
                        String eventTypeName = schema.getName();

                        if (!EsperUtils.eventTypeExists(eventTypeName)) {
                            String epl = "@public @buseventtype @EventRepresentation(avro) create avro schema " + eventTypeName + " as ";
                            StringJoiner joiner = new StringJoiner(",");

                            for (Schema.Field field : schema.getFields()) {
                                joiner.add(field.name() + " " + field.schema().getName());
                            }

                            epl += "(" + joiner.toString() + ")";

                            try {
                                //EsperUtils.addNewSchema(epl);
                                HTTPUtils.updateDeploymentId("eventTypes", record.value().getSchema().getProp("mongodbId"), EsperUtils.addNewSchema(epl), epl);//addNewSimpleEventAPI(schema, EsperUtils.addNewSchema(epl));
                            } catch (EPCompileException | EPDeployException e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (record.value().getSchema().getAliases().contains("epl")) {
                        String epl = record.value().get("epl").toString();
                        logger.info("Sending EPL to Esper: " + epl);
                        try {
                            if (record.value().get("mongodbId") == null)
                                HTTPUtils.addNewPatternAPI(record.value().getSchema(), EsperUtils.createEPL(epl), epl);
                            else
                                HTTPUtils.updateDeploymentId("patterns", record.value().get("mongodbId").toString(), EsperUtils.createEPL(epl), epl);
                        } catch (EPCompileException | EPDeployException e) {
                            logger.error("Error compiling or deploying EPL");
                            e.printStackTrace();
                        }
                    } else if (record.value().getSchema().getAliases().contains("undeploy")) {
                        String undeployIds = record.value().get("undeployIds").toString();
                        EsperUtils.undeployIds(undeployIds);
                    }
            }
        }).start();
    }

    /**
     * @return the configuration for kafka stream app
     */
    private static Properties getConfiguration() {
        Properties props = new Properties();

        props.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, EventSerde.class);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, EventDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerId);

        return props;
    }

}


