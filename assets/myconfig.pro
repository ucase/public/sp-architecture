-injars       kafka-streams-app.jar
-outjars      kafka-streams-app-output.jar
-libraryjars  <java.home>/lib/rt.jar(!**.jar;!module-info.class)

-keepattributes *Annotation*,SourceFile,LineNumberTable
-keep public class processors.Main { *; }
-keep class !processors.**,!serde.** { *; }
