#!/bin/bash
clear
echo "**** STOPPING KAFKA"
sudo bin/kafka-server-stop.sh &
sleep 5
echo "**** STOPPING ZOOKEEPER"
sudo bin/zookeeper-server-stop.sh &
sleep 5
echo "**** DELETING /tmp/kafka-streams"
sudo rm -rf /tmp/kafka-streams
echo "**** DELETING ~/kafka_2.12-2.4.0/logs"
sudo rm -rf ~/kafka_2.12-2.4.0/logs
echo "**** DELETING ~/kafka_2.12-2.4.0/tmp"
sudo rm -rf ~/kafka_2.12-2.4.0/tmp
sleep 5
echo "**** STARTING ZOOKEEPER"
sudo bin/zookeeper-server-start.sh config/zookeeper.properties &
sleep 5
echo "**** STARTING KAFKA"
sudo bin/kafka-server-start.sh config/server.properties &
sleep 5
echo "**** CREATING TOPICS"
sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-input &
sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-output &
sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-schemas &
sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-epl &
sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-undeploy &
exit 1