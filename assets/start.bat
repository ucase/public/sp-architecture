ECHO OFF
ECHO **** DELETING D:\tmp
@RD /S /Q "D:\tmp"
ECHO **** DELETING D:\logs
@RD /S /Q "D:\logs"
ECHO **** DELETING D:\Development\kafka_2.12-2.4.0\tmp
@RD /S /Q "D:\Development\kafka_2.12-2.4.0\tmp"
TIMEOUT /T 5
ECHO **** STARTING ZOOKEEPER
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\zookeeper-server-start.bat D:\Development\kafka_2.12-2.4.0\config\zookeeper.properties
TIMEOUT /T 5
ECHO **** STARTING KAFKA BROKER
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\kafka-server-start.bat D:\Development\kafka_2.12-2.4.0\config\server.properties
TIMEOUT /T 5
ECHO **** CREATING TOPICS
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-output
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-input
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-epl
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-undeploy
START /B D:\Development\kafka_2.12-2.4.0\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-node-js
PAUSE